﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Web.UI;

namespace Akqa.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Render the name and numbers based on the parameters given
        /// </summary>
        /// <param name="Name">capture person name in string value</param>
        /// <param name="Number">number in string value</param>
        /// <returns></returns>
        public ActionResult Index(string Name, string Number)
        {
            /* check the null value for name and number */
            if (!(Name == null || Number == null)) {
                
                /* Make a validation for number input */
                try
                {
                    /* Stor all the Name and Number in ViewBag */
                    ViewBag.Name = Name;
                    double result = Convert.ToDouble(Number);
                    if (result < 0)
                    {
                        ViewBag.Message = "Please enter number(s) on number field.";
                        ViewBag.Number = 0;
                    }
                    else
                    {
                        ViewBag.Number = result.ToString("F");
                    }

                }
                catch (Exception)
                {
                    /* Throw the error when user input wrong value */
                    ViewBag.Message = "Please enter number(s) on number field.";
                }
            }
            /* return to the view */
            return View();
        }
    }
}