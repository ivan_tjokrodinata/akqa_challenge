﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Akqa;
using Akqa.Controllers;

namespace Akqa.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod()]
        public void Index_WithBasicInput()
        {
            // Arrange
            HomeController controller = new HomeController();

            /* act */
            try
            {
                ViewResult result = controller.Index("Ivan", "28") as ViewResult;
            }
            catch (Exception e)
            {
                // Assert
                Assert.Fail(e.Message);
            }
        }

        [TestMethod()]
        public void Index_WithHugeInput()
        {
            // Arrange
            HomeController controller = new HomeController();

            /* act */
            try
            {
                ViewResult result = controller.Index("Ivan", "83021234") as ViewResult;
            }
            catch (Exception e)
            {
                // Assert
                Assert.Fail(e.Message);
            }
        }

        [TestMethod()]
        public void Index_WithUniqueCharactersZeroInput()
        {
            // Arrange
            HomeController controller = new HomeController();

            /* act */
            try
            {
                ViewResult result = controller.Index("Alex's", "00389") as ViewResult;
            }
            catch (Exception e)
            {
                // Assert
                Assert.Fail(e.Message);
            }
        }

        [TestMethod()]
        public void Index_WithCombineCharactersDecimalNumbersInput()
        {
            // Arrange
            HomeController controller = new HomeController();

            /* act */
            try
            {
                ViewResult result = controller.Index("Ivan#28", "390.022") as ViewResult;
            }
            catch (Exception e)
            {
                // Assert
                Assert.Fail(e.Message);
            }
        }

        [TestMethod()]
        public void Index_WithNegativeNumbersInput()
        {
            // Arrange
            HomeController controller = new HomeController();

            /* act */
            try
            {
                ViewResult result = controller.Index("Ivan#28", "-55.92") as ViewResult;
            }
            catch (Exception e)
            {
                // Assert
                Assert.Fail(e.Message);
            }
        }

        [TestMethod()]
        public void Index_WithStringForNumbersInput()
        {
            // Arrange
            HomeController controller = new HomeController();

            /* act */
            try
            {
                ViewResult result = controller.Index("Ivan#28", "testing") as ViewResult;
            }
            catch (Exception e)
            {
                // Assert
                Assert.Fail(e.Message);
            }
        }
    }
}
